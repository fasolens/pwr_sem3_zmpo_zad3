//
// Created by fasolens on 12/15/17.
//

#ifndef PWR_SEM3_ZMPO_ZAD3_1_BOOK_HPP
#define PWR_SEM3_ZMPO_ZAD3_1_BOOK_HPP


#include <string>

class Book {
protected:
    std::string tytul;
    std::string autor;
    int liczba_stron;
public:
    Book() {
        this->tytul = nullptr;
        this->autor = nullptr;
        this->liczba_stron = 0;
    }
    Book(char const* tytul, char const* autor, int l_stron) {
        this->tytul = tytul;
        this->autor = autor;
        this->liczba_stron = l_stron;
    }
    ~Book() {}
    
};


#endif //PWR_SEM3_ZMPO_ZAD3_1_BOOK_HPP

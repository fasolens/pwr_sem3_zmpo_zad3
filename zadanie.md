Zad 1: Proszę napisać klasę Book, tytuł, autor, liczba stron. 
 - Konstruktor parametryczny, 
 - kopiujący, przenoszący, 
 - operator = (kopiujący, przenoszący).  

Statyczne wartości które liczą liczbę wywołań konstruktorów oraz operatorów  
(uwaga: konstruktor i operator kopiujący liczone razem, przenoszące też).  
1.0p

Zad 2: W oparciu o zadanie 1...  
Funkcja sortuje `std::vector` według zadanego komparatora (FUNKCJA, NIE KLASA).  
 - Trzy komparatory (sortują względem tytułu, autora, liczby stron).
 - Implementacja własna sortowania bąbelkowego. 
 - Przeprowadzić sortowanie dla 10 książek, używając wszystkich trzech komparatorów. 
 - Wypisać wartości zmiennych statycznych.

UWAGA 1: W rozwiązanie zadania ma być tylko 1 klasa.  
UWAGA 2: Komparatory i sortowanie NIE SĄ częścią tej klasy.

Na przyszłych zajęciach zgłębiamy zagadnienie operatorów. Proszę mieć ze sobą skończony kod do zadania domowego nr. 2!